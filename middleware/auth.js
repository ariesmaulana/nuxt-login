export default function({ store, error }) {
  if (!store.state.isLogin) {
    error({ statusCode: 403, message: "403 Forbidden" });
  }
}
